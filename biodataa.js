function validateForm(){
        var nama = document.bio.nama.value;
        var nim = document.bio.nim.value;
        var alamat = document.bio.alamat.value;
        var jk = document.bio.jk.value;
        var ket = document.bio.ket.value;
        var regnama = /\d+$/g;
        var regnim =/^\d{10}$/;

    if (nama == "" || regnama.test(nama)) {
        window.alert("Tolong masukan NAMA anda.");
        nama.focus();
        return false;
        }
    if (alamat == "") {
        window.alert("Tolong masukan Alamat anda.");
        alamat.focus();
        return false;
        }
    if (nim == "" || regnim.test(nim)) {
        window.alert("Tolong masukan NIM anda.");
        nim.focus();
        return false;
        }       
    if (ket == "") {
        window.alert("Masukan Keterangan.");
        ket.focus();
        return false;
        }
    if (jk.selectedIndex == -1) {
        alert("Tolong pilih Jenis Kelamin.");
        jk.focus();
        return false;
        }
    return true;
}